package uzytkownicy;

import java.util.List;

import uzytkownicy.managerdatabase.DataBaseManagerAddress;
import uzytkownicy.model.Address;
import uzytkownicy.model.User;

public class RunClass {
	public static void main(String[] args) {
		System.out.println("Start!");

		DataBaseManagerAddress adManagerAddress = new DataBaseManagerAddress();

		List<Address> allAddress = adManagerAddress.getAll();

		for (Address p : allAddress) {
			System.out.println(p.getCountry() + " " + p.getHouseNumber());
		}
		adManagerAddress.deleteById(3);

		Address toUpdate = new Address();

		toUpdate.setCountry("Alaska");
		toUpdate.setHouseNumber("15");

		adManagerAddress.update(toUpdate);
	}
}
