
public class Permission {

	private boolean czyAdmin;
	private boolean czyUser;
	
	public boolean isCzyAdmin() {
		return czyAdmin;
	}
	public void setCzyAdmin(boolean czyAdmin) {
		this.czyAdmin = czyAdmin;
	}
	public boolean isCzyUser() {
		return czyUser;
	}
	public void setCzyUser(boolean czyUser) {
		this.czyUser = czyUser;
	}
	
}
