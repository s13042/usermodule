
public class Role {

	private boolean czyPracownik;
	private boolean czyGlupi;
	
	public boolean isCzyPracownik() {
		return czyPracownik;
	}
	public void setCzyPracownik(boolean czyPracownik) {
		this.czyPracownik = czyPracownik;
	}
	public boolean isCzyGlupi() {
		return czyGlupi;
	}
	public void setCzyGlupi(boolean czyGlupi) {
		this.czyGlupi = czyGlupi;
	}
}
